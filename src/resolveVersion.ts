import { findConflictStartIndex, getTaskId, getVersion, getVersionReg } from './utils'

declare global {
    interface Array<T> {
        spliceClean: Array<T>['splice'];
    }
}

Array.prototype.spliceClean = function splice<T>(start: number, deleteCount: number, ...items: Array<T>): Array<T>
{
    const tmp = [...this]
    Array.prototype.splice.call(tmp, start, deleteCount, ...items)
    return tmp
}

/**
 * @param content - file content which contains version conflicts
 * @return - Tuple where first element is new content with resolved string, and second is new version line
 */
export const resolveVersion = (content: string): [string, string] =>
{
    const lines = content.split('\n')

    const conflictStartLine = findConflictStartIndex(lines)
    if (conflictStartLine === -1) throw 'nothing to resolve'
    const versionLine = lines[conflictStartLine + 1]

    const currentVersion = getVersion(lines[conflictStartLine + 1])
    if (currentVersion == null) throw 'cannot find current version'
    const taskId = getTaskId(currentVersion)
    if (taskId == null) throw 'cannot find taskId of current version'

    const newVersion = getVersion(lines[conflictStartLine + 3])
    if (newVersion == null) throw 'cannot find new version'

    const combinedVersion = newVersion // 1.12.1-SNAPSHOT
        .split('-') // ['1.12.1' 'SNAPSHOT']
        .spliceClean(1, 0, taskId) // ['1.12.1', 'COM-1', 'SNAPSHOT']
        .join('-') // '1.12.1-COM-1-SNAPSHOT'

    const resultVersionLine = versionLine.replace(getVersionReg, combinedVersion)

    lines.splice(conflictStartLine, 5, resultVersionLine)

    return [lines.join('\n'), resultVersionLine]
}
