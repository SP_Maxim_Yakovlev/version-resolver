import { readFile, writeFile } from 'fs/promises'

import {
    getConfig,
    getParams,
    getUnmergedFiles,
    gitAddFiles,
    green,
    red,
    shellPrompt,
    startReadStdin,
} from './shellUtils'
import { resolveVersion } from './resolveVersion'
import { groupByStatus } from './utils'

const rl = startReadStdin()

export const finalize = async (fulfilled: Array<string>, rejected: Array<string>) =>
{
    const couldAdd = fulfilled.length > 0
    const hasRejected = rejected.length > 0

    if (!hasRejected) console.log(`All ${green(fulfilled.length + ' conflict files')} was resolved!\n`)
    else if (!couldAdd) console.log(`There are ${red('no resolved files :(')}\n`)
    else console.log(`There are ${green(fulfilled.length + ' resolved files')}\nAnd ${red(rejected.length + ' unresolved')}\n`)

    if (couldAdd) console.log(green(`Resolved:\n${fulfilled.join('\n')}`))
    if (hasRejected) console.log(red(`Rejected:\n${rejected.join('\n')}`))

    if (couldAdd)
    {
        const answer = await shellPrompt(rl, '\nWould you like to add(git add) all resolved files?(Y/n) ')
        if ((/y/i).test(answer))
        {
            await gitAddFiles(fulfilled)
            console.log('\n\ndone!')
        }
        else
        {
            console.log('\n\nok)')
        }
    }
    else
    {
        console.log('\nSeems like you\'re should resolve it by yourself...')
    }
}

export function exit(code?: number)
{
    if (process.env.NODE_ENV !== 'test')
    {
        rl.close()
        process.exit(code)
    }
}

export async function start()
{
    const unmergedFiles = await getUnmergedFiles().catch(() => null)
    if (unmergedFiles === null)
    {
        console.log(red('Problems with getting unmerged files, are you sure this directory is git repository?'))
        exit(1)
        return // assure the typescript that the program will definitely stop
    }
    if (unmergedFiles.length === 0)
    {
        console.log(red('There is no unmerged conflict files!'))
        exit(1)
    }
    const [params, unknownParams] = getParams()
    if (unknownParams.length > 0)
    {
        console.log(red(`Unknown params: ${unknownParams.join(' ')}`))
        exit(1)
    }

    const config = await getConfig(params.configFile)

    const files = config && config.filesToEdit && config.filesToEdit.length > 0
        /* There is seems to be a bug in typescript...
         * Above we check is filesToEdit not null, and it's applied when we check config.filesToEdit.length
         * But in callback fn, by some reason, it's can be null?
         */
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        ? unmergedFiles.filter(file => config.filesToEdit.includes(file))
        : unmergedFiles

    const result = await Promise.allSettled(files.map(
        (file: string) => (
            readFile(file, 'utf8')
                .then(resolveVersion)
                .then(async ([newContent, resultLine]) =>
                {
                    await writeFile(file, newContent, 'utf8')

                    console.log(`Resolve ${file}, now version line is ${resultLine.trim()}`)
                    return file
                })
                .catch(err =>
                {
                    console.log(`Cannot resolve ${file} because of: ${err.toString()}`)
                    throw file
                })
        ),
    ))

    const { fulfilled, rejected } = groupByStatus(result)

    await finalize(fulfilled, rejected)
    exit(0)
}

if (process.env.NODE_ENV !== 'test')
{
    start()
}
