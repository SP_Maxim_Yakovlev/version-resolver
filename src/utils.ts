export const findConflictStartIndex = (lines: Array<string>) => (
    lines.findIndex(line => line.startsWith('<<<<<<< HEAD'))
)

export const getVersionReg = /[0-9]+\..*SNAPSHOT/
export const getVersion = (line: string) => getVersionReg.exec(line)?.[0]

export const getTaskIdReg = /[0-9]+\.[0-9]+\.[0-9]+-(.*)-SNAPSHOT/
export const getTaskId = (version: string) => (
    getTaskIdReg.exec(version)?.[1]
)

interface GroupByStatusResult<T> {
    fulfilled: Array<T>;
    rejected: Array<T>;
}
export const groupByStatus = <T>(promiseChainResults: Array<PromiseSettledResult<T>>): GroupByStatusResult<T> => (
    promiseChainResults.reduce<GroupByStatusResult<T>>(
        (acc, res) => ({
            fulfilled: acc.fulfilled.concat(res.status === 'fulfilled' ? res.value : []),
            rejected: acc.rejected.concat(res.status === 'rejected' ? res.reason : []),
        }), { fulfilled: [], rejected: [] },
    )
)
