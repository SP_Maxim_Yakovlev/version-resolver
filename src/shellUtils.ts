import { promisify } from 'util'
import { exec as callbackExec } from 'child_process'
import { createInterface, Interface as ReadlineInterface } from 'readline'
import { readFile } from 'fs/promises'

export const p_exec = promisify(callbackExec)

export const getUnmergedFiles = () => (
    p_exec('git diff --name-only --diff-filter=U')
        .then(res => res.stdout.split('\n').slice(0, -1))
)
export const gitAddFiles = (files: Array<string>) => p_exec(`git add ${files.join(' ')}`)

export const resetESC = '\u001b[0m'
export const green = (text: string) => '\u001b[32;1m' + text + resetESC
export const red = (text: string) => '\u001b[31;1m' + text + resetESC

export const startReadStdin = () => createInterface({
    input: process.stdin,
    output: process.stdout,
})

export const shellPrompt = (rl: ReadlineInterface, question: string) => new Promise<string>((res) =>
{
    rl.question(question, (answer =>
    {
        res(answer)
    }))
})

const allowedParams = [
    '--configFile',
]
interface Params {
    configFile?: string | undefined;
}
/**
 * Get params from shell
 * @returns First item is object with listed params, second is list of unknown params
 */
export const getParams = (): [Params, Array<string>] =>
{
    const paramsList = process.argv.slice(2)
    const unknownParams: Array<string> = []

    const params: Params = {} as Params
    for (let i = 0; i < paramsList.length; i++)
    {
        const param = paramsList[i]
        const nextParam = paramsList[i + 1]
        if (allowedParams.includes(param))
        {
            params[param.slice(2) as keyof Params] = nextParam
            i++ // skip next param
        }
        else
        {
            unknownParams.push(param)
        }
    }

    return [params, unknownParams]
}

interface Config {
    filesToEdit?: Array<string>;
}
export const getConfig = async (configPath = './resolve-version-config.json'): Promise<Config | null> => (
    readFile(configPath, 'utf-8')
        .then(JSON.parse)
        .catch(() => null)
)
