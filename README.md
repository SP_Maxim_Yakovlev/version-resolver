# version-resolver
Resolve versions conflicts

## Usage
Type `version-resolver` in directory with git conflicts

## Installation
Required: `npm >= 5.0.0`, `node >= 14.0.0`

Type `npm install -g git+https://SP_Maxim_Yakovlev@bitbucket.org/SP_Maxim_Yakovlev/version-resolver.git`


In case you install global scripts in first time on Windows, be sure `npm` binaries is added in `PATH`. If not - add to `%AppData%\npm` to your environment path

In case you using Windows, npm cannot properly install global scripts from a git. So open `%AppData%\npm`, and replace files 
```
version-resolver
version-resolver.cmd
version-resolver.ps1
```
with files from `./startScripts` directory in this repository.

You also should do this after updates. Also, npm by some reasons download full repository to `%AppData%\npm\node_modules\version-resolver`, so you can get `startScripts` from there  

When this scripts will be posted to npm repository, the above problem should be disappeared 
