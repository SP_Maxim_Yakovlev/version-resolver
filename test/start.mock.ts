export const conflictContentYaml
    = `key: someValue
<<<<<<< HEAD',
version: 1.11.1-COM-1-SNAPSHOT
=======
version: 1.12.1-SNAPSHOT
>>>>>>> master`
export const resolvedContentYaml
    = `key: someValue
version: 1.12.1-COM-1-SNAPSHOT`

export const conflictContentXml
    = `<key>value</key>
<<<<<<< HEAD',
<version>1.11.1-COM-1-SNAPSHOT</version>
=======
<version>1.12.1-SNAPSHOT</version>
>>>>>>> master`
export const resolvedContentXml
    = `<key>value</key>
<version>1.12.1-COM-1-SNAPSHOT</version>`

export const unmergedFiles = {
    fileYaml1: conflictContentYaml,
    fileYaml2: conflictContentYaml,
    fileXml1: conflictContentXml,
    fileXml2: conflictContentXml,
}
export const resolvedFiles = {
    fileYaml1: resolvedContentYaml,
    fileYaml2: resolvedContentYaml,
    fileXml1: resolvedContentXml,
    fileXml2: resolvedContentXml,
}
