import * as utils from '@src/utils'

describe('utils', () =>
{
    it('findConflictStartIndex returns index of line where conflict starts', () =>
    {
        const lines = [
            'Start',
            '<<<<<<< HEAD\n',
            'this is some content to mess with',
            '=======',
            'totally different content to merge later',
            '>>>>>>> new_branch_to_merge_later',
        ]

        expect(utils.findConflictStartIndex(lines)).toBe(1)
    })
    it('getVersion returns correct version', () =>
    {
        expect(utils.getVersion('version: 1.11.1-COM-1-SNAPSHOT')).toBe('1.11.1-COM-1-SNAPSHOT')
        expect(utils.getVersion('version: 1.11.1-SNAPSHOT')).toBe('1.11.1-SNAPSHOT')
        expect(utils.getVersion('<version>1.11.1-SNAPSHOT</version>>')).toBe('1.11.1-SNAPSHOT')
    })

    it('getTaskId returns correct taskId from version line', () =>
    {
        expect(utils.getTaskId('1.11.1-COM-1-SNAPSHOT')).toBe('COM-1')
    })

    it('groupByStatus returns grouped object by status', () =>
    {
        const ungroupedArray: Array<PromiseSettledResult<string>> = [
            { status: 'fulfilled', value: 'some-val1' }, { status: 'fulfilled', value: 'some-val2' },
            { status: 'rejected', reason: 'some-reason1' }, { status: 'rejected', reason: 'some-reason2' },
        ]

        expect(utils.groupByStatus(ungroupedArray)).toEqual({
            fulfilled: ['some-val1', 'some-val2'],
            rejected: ['some-reason1', 'some-reason2'],
        })
    })
})
