import { resolveVersion } from '@src/resolveVersion'

describe('resolveVersion', () =>
{
    it('resolveVersion resolves conflicts with merging versions', () =>
    {
        const conflictContent
            = `key: someValue
<<<<<<< HEAD',
version: 1.11.1-COM-1-SNAPSHOT
=======
version: 1.12.1-SNAPSHOT
>>>>>>> master`
        const resolvedContent
            = `key: someValue
version: 1.12.1-COM-1-SNAPSHOT`

        expect(resolveVersion(conflictContent)).toEqual([resolvedContent, 'version: 1.12.1-COM-1-SNAPSHOT'])
    })
})
