import * as main from '@src/index'
import * as shellUtils from '@src/shellUtils'
import * as fs from 'fs/promises'

import {
    unmergedFiles,
    resolvedFiles,
    conflictContentYaml,
    conflictContentXml,
    resolvedContentXml,
    resolvedContentYaml,
} from './start.mock'
import { Interface } from 'readline'

let unmerged = { ...unmergedFiles } // mutable
let consoleOutput = ''
describe('index.ts start function', () =>
{
    beforeAll(() =>
    {
        jest.spyOn(main, 'exit').mockImplementation(() => undefined)
        jest.spyOn(shellUtils, 'startReadStdin').mockImplementation(() => ({} as Interface))
        jest.spyOn(console, 'log').mockImplementation((...values) =>
        {
            consoleOutput += values.join('\n') + '\n'
        })
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        jest.spyOn(main, 'finalize').mockImplementation(async (_, __) => undefined)

        jest.spyOn(fs, 'readFile').mockImplementation(async (path: keyof typeof unmerged) => unmerged[path])
        jest.spyOn(fs, 'writeFile').mockImplementation(async (path: keyof typeof unmerged, content: string) =>
        {
            unmerged[path] = content
        })
    })
    beforeEach(() =>
    {
        jest.spyOn(shellUtils, 'getUnmergedFiles').mockResolvedValue(Object.keys(unmerged))
        jest.spyOn(shellUtils, 'getParams').mockReturnValue([{}, []])
        jest.spyOn(shellUtils, 'getConfig').mockResolvedValue({})
    })
    afterEach(() =>
    {
        consoleOutput = ''
        unmerged = { ...unmergedFiles }
    })

    it('resolves all unmerged files', async () =>
    {
        await main.start()

        expect(unmerged).toEqual(resolvedFiles)

        expect(main.finalize).toBeCalledWith(
            Object.keys(unmerged),
            [],
        )

        expect(consoleOutput).toBe(
            `\
Resolve fileYaml1, now version line is version: 1.12.1-COM-1-SNAPSHOT
Resolve fileYaml2, now version line is version: 1.12.1-COM-1-SNAPSHOT
Resolve fileXml1, now version line is <version>1.12.1-COM-1-SNAPSHOT</version>
Resolve fileXml2, now version line is <version>1.12.1-COM-1-SNAPSHOT</version>
`,
        )
    })

    it('resolves only provided by config unmerged files', async () =>
    {
        const filesToEdit = [
            'fileYaml1',
            'fileXml1',
        ]
        jest.spyOn(shellUtils, 'getConfig').mockResolvedValue({ filesToEdit })
        await main.start()

        expect(unmerged).toEqual({
            fileYaml1: resolvedContentYaml,
            fileYaml2: conflictContentYaml,
            fileXml1: resolvedContentXml,
            fileXml2: conflictContentXml,
        })

        expect(main.finalize).toBeCalledWith(
            filesToEdit,
            [],
        )

        expect(consoleOutput).toBe(
            `\
Resolve fileYaml1, now version line is version: 1.12.1-COM-1-SNAPSHOT
Resolve fileXml1, now version line is <version>1.12.1-COM-1-SNAPSHOT</version>
`,
        )
    })
})
