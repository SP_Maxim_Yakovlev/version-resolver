import * as main from '@src/index'
import * as shellUtils from '@src/shellUtils'

const { green, red } = shellUtils
let consoleOutput = ''
describe('index.ts finalize function', () =>
{
    beforeAll(() =>
    {
        jest.spyOn(console, 'log').mockImplementation((...values) =>
        {
            consoleOutput += values.join('\n') + '\n'
        })
        jest.spyOn(shellUtils, 'shellPrompt').mockImplementation(async (rl, question) =>
        {
            consoleOutput += question + 'y'
            return 'y'
        })
        jest.spyOn(shellUtils, 'p_exec').mockResolvedValue({ stdout: '', stderr: '' })
    })
    afterEach(() =>
    {
        consoleOutput = ''
    })

    it('prints correct information on all fulfilled', async () =>
    {
        const gitAddFiles = jest.spyOn(shellUtils, 'gitAddFiles')

        await main.finalize(['file1', 'file2'], [])

        expect(consoleOutput).toBe(
            `\
All ${shellUtils.green('2 conflict files')} was resolved!

${green(`Resolved:
file1\nfile2`)}

Would you like to add(git add) all resolved files?(Y/n) y

done!
`,
        )

        expect(gitAddFiles).toBeCalledWith(['file1', 'file2'])
        gitAddFiles.mockClear()
    })

    it('prints correct information on all fulfilled and not accept git add', async () =>
    {
        jest.spyOn(shellUtils, 'shellPrompt').mockImplementationOnce(async (rl, question) =>
        {
            consoleOutput += question + 'n'
            return 'n'
        })
        const gitAddFiles = jest.spyOn(shellUtils, 'gitAddFiles')

        await main.finalize(['file1', 'file2'], [])

        expect(consoleOutput).toBe(
            `\
All ${shellUtils.green('2 conflict files')} was resolved!

${green(`Resolved:
file1\nfile2`)}

Would you like to add(git add) all resolved files?(Y/n) n

ok)
`,
        )

        expect(gitAddFiles).not.toBeCalled()
        gitAddFiles.mockClear()
    })

    it('prints correct information on some fulfilled and some rejected', async () =>
    {
        const gitAddFiles = jest.spyOn(shellUtils, 'gitAddFiles')

        await main.finalize(['ful1', 'ful2'], ['rej1', 'rej2'])

        expect(consoleOutput).toBe(
            `\
There are ${green('2 resolved files')}
And ${red('2 unresolved')}

${green(`Resolved:
ful1\nful2`)}
${red(`Rejected:
rej1\nrej2`)}

Would you like to add(git add) all resolved files?(Y/n) y

done!
`,
        )
        expect(gitAddFiles).toBeCalledWith(['ful1', 'ful2'])
        gitAddFiles.mockClear()
    })

    it('prints correct information on all rejected', async() =>
    {
        const gitAddFiles = jest.spyOn(shellUtils, 'gitAddFiles')

        await main.finalize([], ['rej1', 'rej2'])

        expect(consoleOutput).toBe(
            `\
There are ${red('no resolved files :(')}

${red(`Rejected:
rej1\nrej2`)}

Seems like you're should resolve it by yourself...
`,
        )

        expect(gitAddFiles).not.toBeCalled()
        gitAddFiles.mockClear()
    })
})
