import * as shellUtils from '@src/shellUtils'
import * as fs from 'fs/promises'

describe('shellUtils', () =>
{
    it('getUnmergedFiles returns array of files', async () =>
    {
        const unmergedFilesMock = `file1\nfile2\nfile3\nfile4\n`
        jest.spyOn(shellUtils, 'p_exec').mockResolvedValue({ stdout: unmergedFilesMock, stderr: '' })

        const result = await shellUtils.getUnmergedFiles()
        expect(result).toEqual(['file1', 'file2', 'file3', 'file4'])
    })

    it('getParams returns params and unknown params', () =>
    {
        process.argv = ['node', 'executable.js', '--configFile', 'path', 'unknown1', 'unknown2']

        expect(shellUtils.getParams()).toEqual([{ configFile: 'path' }, ['unknown1', 'unknown2']])
    })

    it('getConfig returns config object', async () =>
    {
        jest.spyOn(fs, 'readFile').mockResolvedValue(`{ "filesToEdit": ["file1", "file2"] }`)

        const result = await shellUtils.getConfig()
        expect(result).toEqual({ filesToEdit: ['file1', 'file2'] })
    })
})
